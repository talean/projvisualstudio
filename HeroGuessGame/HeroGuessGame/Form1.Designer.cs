﻿
namespace HeroGuessGame
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Question = new System.Windows.Forms.Label();
            this.CheckAnswer = new System.Windows.Forms.Button();
            this.Answer1 = new System.Windows.Forms.Label();
            this.Answer2 = new System.Windows.Forms.Label();
            this.Answer3 = new System.Windows.Forms.Label();
            this.Answer4 = new System.Windows.Forms.Label();
            this.ScoreLabel = new System.Windows.Forms.Label();
            this.TitleText = new System.Windows.Forms.Label();
            this.TitlePicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.TitlePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // Question
            // 
            this.Question.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Question.Location = new System.Drawing.Point(105, 75);
            this.Question.Name = "Question";
            this.Question.Size = new System.Drawing.Size(352, 98);
            this.Question.TabIndex = 0;
            this.Question.Text = "label1";
            // 
            // CheckAnswer
            // 
            this.CheckAnswer.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CheckAnswer.Location = new System.Drawing.Point(175, 470);
            this.CheckAnswer.Name = "CheckAnswer";
            this.CheckAnswer.Size = new System.Drawing.Size(200, 65);
            this.CheckAnswer.TabIndex = 1;
            this.CheckAnswer.Text = "Проверить ответ";
            this.CheckAnswer.UseVisualStyleBackColor = true;
            this.CheckAnswer.Click += new System.EventHandler(this.CheckAnswer_Click);
            // 
            // Answer1
            // 
            this.Answer1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Answer1.Location = new System.Drawing.Point(105, 275);
            this.Answer1.Name = "Answer1";
            this.Answer1.Size = new System.Drawing.Size(160, 50);
            this.Answer1.TabIndex = 2;
            this.Answer1.Text = "label2";
            this.Answer1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Answer2
            // 
            this.Answer2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Answer2.Location = new System.Drawing.Point(105, 383);
            this.Answer2.Name = "Answer2";
            this.Answer2.Size = new System.Drawing.Size(160, 50);
            this.Answer2.TabIndex = 3;
            this.Answer2.Text = "label3";
            this.Answer2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Answer3
            // 
            this.Answer3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Answer3.Location = new System.Drawing.Point(297, 275);
            this.Answer3.Name = "Answer3";
            this.Answer3.Size = new System.Drawing.Size(160, 50);
            this.Answer3.TabIndex = 4;
            this.Answer3.Text = "label4";
            this.Answer3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Answer4
            // 
            this.Answer4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Answer4.Location = new System.Drawing.Point(297, 383);
            this.Answer4.Name = "Answer4";
            this.Answer4.Size = new System.Drawing.Size(160, 50);
            this.Answer4.TabIndex = 5;
            this.Answer4.Text = "label5";
            this.Answer4.Click += new System.EventHandler(this.label4_Click);
            // 
            // ScoreLabel
            // 
            this.ScoreLabel.BackColor = System.Drawing.Color.Transparent;
            this.ScoreLabel.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ScoreLabel.Location = new System.Drawing.Point(537, 75);
            this.ScoreLabel.Name = "ScoreLabel";
            this.ScoreLabel.Size = new System.Drawing.Size(330, 31);
            this.ScoreLabel.TabIndex = 6;
            this.ScoreLabel.Text = "Правильно решено: X";
            this.ScoreLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TitleText
            // 
            this.TitleText.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TitleText.Location = new System.Drawing.Point(537, 297);
            this.TitleText.Name = "TitleText";
            this.TitleText.Size = new System.Drawing.Size(330, 238);
            this.TitleText.TabIndex = 7;
            this.TitleText.Text = "label6";
            // 
            // TitlePicture
            // 
            this.TitlePicture.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("TitlePicture.BackgroundImage")));
            this.TitlePicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.TitlePicture.Location = new System.Drawing.Point(537, 109);
            this.TitlePicture.Name = "TitlePicture";
            this.TitlePicture.Size = new System.Drawing.Size(330, 185);
            this.TitlePicture.TabIndex = 8;
            this.TitlePicture.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(975, 589);
            this.Controls.Add(this.TitlePicture);
            this.Controls.Add(this.TitleText);
            this.Controls.Add(this.ScoreLabel);
            this.Controls.Add(this.Answer4);
            this.Controls.Add(this.Answer3);
            this.Controls.Add(this.Answer2);
            this.Controls.Add(this.Answer1);
            this.Controls.Add(this.CheckAnswer);
            this.Controls.Add(this.Question);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(991, 628);
            this.MinimumSize = new System.Drawing.Size(991, 628);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.TitlePicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Question;
        private System.Windows.Forms.Button CheckAnswer;
        private System.Windows.Forms.Label Answer1;
        private System.Windows.Forms.Label Answer2;
        private System.Windows.Forms.Label Answer3;
        private System.Windows.Forms.Label Answer4;
        private System.Windows.Forms.Label TitleText;
        private System.Windows.Forms.PictureBox TitlePicture;
        public System.Windows.Forms.Label ScoreLabel;
    }
}

