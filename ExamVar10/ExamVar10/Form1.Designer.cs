﻿
namespace ExamVar10
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartGame = new System.Windows.Forms.Button();
            this.TriesCounterLabel = new System.Windows.Forms.Label();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.GamesCounter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StartGame
            // 
            this.StartGame.Location = new System.Drawing.Point(207, 361);
            this.StartGame.Name = "StartGame";
            this.StartGame.Size = new System.Drawing.Size(140, 23);
            this.StartGame.TabIndex = 0;
            this.StartGame.Text = "Начать игру";
            this.StartGame.UseVisualStyleBackColor = true;
            this.StartGame.Click += new System.EventHandler(this.StartGame_Click);
            // 
            // TriesCounterLabel
            // 
            this.TriesCounterLabel.Location = new System.Drawing.Point(630, 37);
            this.TriesCounterLabel.Name = "TriesCounterLabel";
            this.TriesCounterLabel.Size = new System.Drawing.Size(140, 32);
            this.TriesCounterLabel.TabIndex = 1;
            this.TriesCounterLabel.Text = "label1";
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Location = new System.Drawing.Point(630, 87);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(38, 15);
            this.TimeLabel.TabIndex = 2;
            this.TimeLabel.Text = "label1";
            // 
            // GamesCounter
            // 
            this.GamesCounter.Location = new System.Drawing.Point(207, 402);
            this.GamesCounter.Name = "GamesCounter";
            this.GamesCounter.Size = new System.Drawing.Size(140, 23);
            this.GamesCounter.TabIndex = 3;
            this.GamesCounter.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(849, 568);
            this.Controls.Add(this.GamesCounter);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.TriesCounterLabel);
            this.Controls.Add(this.StartGame);
            this.Name = "Form1";
            this.Text = "Form1";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartGame;
        private System.Windows.Forms.Label TriesCounterLabel;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.Label GamesCounter;
    }
}

