﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroGuessGame
{
    public partial class Form1 : Form
    {
        int TaskNumber = 0;
        int Score = 0;
        int ChosenAnswer = 0;
        Task[] Tasks = new Task[12];
        int ClicksAmount = 0;
        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(99, 39, 15);
            UpdateScore();
            ClearLabels();
            CreateTasks(Tasks);
            FillLabels();
            CheckAnswer.Text = "Проверить ответ";
            TitleText.BackColor = Color.White;
            TitleText.Text = "Классическая русская литература - это образцовые произведения, написанные в разные исторические эпохи русскоязычными писателями. С большинством произведений мы знакомы ещё со школьных времен. Данное приложение создано для того, чтобы проверить знания об основных героях культовых произведений русской классической литературы. Для того, чтобы ответить на вопрос выберите к какому произведению принадлежит данный герой, кликнув на соответствующую кнопку и нажмите проверить ответ";

        }

        private void UpdateScore()
        {
            ScoreLabel.Text = "Правильно решено: " + Score.ToString();
        }

        private void FillLabels()
        {
            Question.BackColor = Color.White;
            Question.Text = Tasks[TaskNumber].Question;
            Answer1.Text = Tasks[TaskNumber].Answer1;
            Answer2.Text = Tasks[TaskNumber].Answer2;
            Answer3.Text = Tasks[TaskNumber].Answer3;
            Answer4.Text = Tasks[TaskNumber].Answer4;
           
        }

        private void CheckAnswer_Click(object sender, EventArgs e)
        {
            if (ChosenAnswer == 0)
            { }
            else {
                ClicksAmount++;
                CheckAnswer.Text = "Следующий вопрос";
                if (ClicksAmount % 2 == 1)
                {
                    switch (Tasks[TaskNumber].RightAnswer)
                    {
                        case "1":

                            if (ChosenAnswer == 1)
                            {
                                Score++;
                            }
                            else
                            {
                                switch (ChosenAnswer)
                                {
                                    case 2:
                                        Answer2.BackColor = Color.Red;
                                        break;
                                    case 3:
                                        Answer3.BackColor = Color.Red;
                                        break;
                                    case 4:
                                        Answer4.BackColor = Color.Red;
                                        break;
                                }
                            }
                            Answer1.BackColor = Color.Green;
                            break;

                        case "2":
                            if (ChosenAnswer == 2)
                            {
                                Score++;
                            }
                            else
                            {
                                switch (ChosenAnswer)
                                {
                                    case 1:
                                        Answer1.BackColor = Color.Red;
                                        break;
                                    case 3:
                                        Answer3.BackColor = Color.Red;
                                        break;
                                    case 4:
                                        Answer4.BackColor = Color.Red;
                                        break;
                                }
                            }
                            Answer2.BackColor = Color.Green;
                            break;

                        case "3":
                            if (ChosenAnswer == 3)
                            {
                                Score++;
                            }
                            else
                            {
                                switch (ChosenAnswer)
                                {
                                    case 1:
                                        Answer1.BackColor = Color.Red;
                                        break;
                                    case 2:
                                        Answer2.BackColor = Color.Red;
                                        break;
                                    case 4:
                                        Answer4.BackColor = Color.Red;
                                        break;
                                }
                            }
                            Answer3.BackColor = Color.Green;
                            break;

                        case "4":
                            if (ChosenAnswer == 4)
                            {
                                Score++;
                            }
                            else
                            {
                                switch (ChosenAnswer)
                                {
                                    case 1:
                                        Answer1.BackColor = Color.Red;
                                        break;
                                    case 2:
                                        Answer2.BackColor = Color.Red;
                                        break;
                                    case 3:
                                        Answer3.BackColor = Color.Red;
                                        break;
                                }
                            }
                            Answer4.BackColor = Color.Green;
                            break;
                    }
                }
                UpdateScore();
                if (ClicksAmount%2 == 0)
                    NextQuestion();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (ClicksAmount % 2 == 0)
            {
                ChosenAnswer = 0;
                ClearLabels();
                Answer1.BackColor = Color.Yellow;
                ChosenAnswer = 1;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            if (ClicksAmount % 2 == 0)
            {
                ChosenAnswer = 0;
                ClearLabels();
                Answer2.BackColor = Color.Yellow;
                ChosenAnswer = 2;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            if (ClicksAmount % 2 == 0)
            {
                ChosenAnswer = 0;
                ClearLabels();
                Answer3.BackColor = Color.Yellow;
                ChosenAnswer = 3;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            if (ClicksAmount % 2 == 0)
            {
                ChosenAnswer = 0;
                ClearLabels();
                Answer4.BackColor = Color.Yellow;
                ChosenAnswer = 4;
            }
        }

        private void ClearLabels()
        {
            Answer1.BackColor = Color.White;
            Answer2.BackColor = Color.White;
            Answer3.BackColor = Color.White;
            Answer4.BackColor = Color.White;
        }

        private void NextQuestion()
        {
            ChosenAnswer = 0;
            TaskNumber++;
            if (TaskNumber > 11)
            {
                DialogResult dialog = MessageBox.Show("Спасибо за игру!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                this.Close();
            }
            else
            {
                CheckAnswer.Text = "Проверить ответ";
                ClearLabels();
                FillLabels();
            }
        }

        private void CreateTasks(Task[] Tasks)
        {
            for (int i=0; i<Tasks.Length; i++)
            {
                Tasks[i] = new Task();
            }
            
            Tasks[0].Question = "В каком из нижеперечисленных произведений Григорий Печорин является главным героем?";
            Tasks[0].Answer1 = "Герой нашего времени (Михаил Лермонтов)";
            Tasks[0].Answer2 = "Женитьба (Николай Гоголь)";
            Tasks[0].Answer3 = "Белая гвардия (Михаил Булгаков)";
            Tasks[0].Answer4 = "Записки из подполья (Фёдор Достоевский)";
            Tasks[0].RightAnswer = "1";

            Tasks[1].Question = "В каком из нижеперечисленных произведений Илья Обломов является главным героем?";
            Tasks[1].Answer1 = "Село Степанчиково и его обитатели (Федор Достоевский)";
            Tasks[1].Answer2 = "Обломов (Иван Гончаров)";
            Tasks[1].Answer3 = "Отцы и дети (Иван Тургенев)";
            Tasks[1].Answer4 = "Игрок (Федор Достоевский)";
            Tasks[1].RightAnswer = "2";

            Tasks[2].Question = "Главной героиней какого из нижеперечисленных произведений является Вера Шеина?";
            Tasks[2].Answer1 = "Леди Макбет Мценского уезда (Николай Лесков)";
            Tasks[2].Answer2 = "Дама с собачкой (Антон Чехов)";
            Tasks[2].Answer3 = "Гранатовый браслет (Александр Куприн)";
            Tasks[2].Answer4 = "Темные аллеи (Иван Бунин)";
            Tasks[2].RightAnswer = "3";

            Tasks[3].Question = "Главной героиней какого из нижеперечисленных произведений является Лариса Дмитриевна?";
            Tasks[3].Answer1 = "Женитьба (Николай Гоголь)";
            Tasks[3].Answer2 = "Каштанка (Антон Чехов)";
            Tasks[3].Answer3 = "Бесприданница (Александр Островский)";
            Tasks[3].Answer4 = "Темные аллеи (Иван Бунин)";
            Tasks[3].RightAnswer = "3";

            Tasks[4].Question = "В каком из нижеперечисленных произведений Васков Федот является главным героем?";
            Tasks[4].Answer1 = "Капитанская дочка (Александр Пушкин)";
            Tasks[4].Answer2 = "А зори здесь тихие (Борис Васильев)";
            Tasks[4].Answer3 = "Село Степанчиково и его обитатели (Федор Достоевский)";
            Tasks[4].Answer4 = "Драма на охоте (Антон Чехов) ";
            Tasks[4].RightAnswer = "2";

            Tasks[5].Question = "В каком из нижеперечисленных произведений Григорий Отрепьев является одним из главных героев?";
            Tasks[5].Answer1 = "Дядя Ваня (Антон Чехов)";
            Tasks[5].Answer2 = "Два капитана (Вениамин Каверин)";
            Tasks[5].Answer3 = "Нос (Николай Гоголь)";
            Tasks[5].Answer4 = "Борис Годунов (Александр Пушкин)";
            Tasks[5].RightAnswer = "4";

            Tasks[6].Question = "Главной героиней какого из нижеперечисленных произведений является Татьяна Ларина?";
            Tasks[6].Answer1 = "Бесприданница (Александр Островский)";
            Tasks[6].Answer2 = "Евгений Онегин (Александр Пушкин)";
            Tasks[6].Answer3 = "Пиковая Дама (Александр Пушкин)";
            Tasks[6].Answer4 = "Бесы (Федор Достоевский)";
            Tasks[6].RightAnswer = "2";

            Tasks[7].Question = "Главной героиней какого из нижеперечисленных произведений является Мария Троекурова?";
            Tasks[7].Answer1 = "Три сестры (Антон Чехов)";
            Tasks[7].Answer2 = "Белые ночи (Федор Достоевский)";
            Tasks[7].Answer3 = "Дворянское гнездо (Иван Тургенев) ";
            Tasks[7].Answer4 = "Дубровский (Александр Пушкин)";
            Tasks[7].RightAnswer = "4";

            Tasks[8].Question = "В каком из нижеперечисленных произведений Полиграф Шариков является одним из главных героев?";
            Tasks[8].Answer1 = "Собачье сердце (Михаил Булгаков)";
            Tasks[8].Answer2 = "Герой нашего времени (Михаил Лермонтов)";
            Tasks[8].Answer3 = "Фальшивый купон (Лев Толстой)";
            Tasks[8].Answer4 = "Повести Белкина (Александр Куприн)";
            Tasks[8].RightAnswer = "1";

            Tasks[9].Question = "В каком из нижеперечисленных произведений Андрей Болконский является одним из главных героев?";
            Tasks[9].Answer1 = "Евгений Онегин (Александр Пушкин)";
            Tasks[9].Answer2 = "Муму (Иван Тургенев)";
            Tasks[9].Answer3 = "Олеся (Александр Куприн)";
            Tasks[9].Answer4 = "Война и мир (Лев Толстой)";
            Tasks[9].RightAnswer = "4";

            Tasks[10].Question = "В каком из нижеперечисленных произведений Петр Гринев является главным героем?";
            Tasks[10].Answer1 = "Капитанская дочка (Александр Пушкин)";
            Tasks[10].Answer2 = "Отцы и дети (Иван Тургенев)";
            Tasks[10].Answer3 = "Казаки (Лев Толстой)";
            Tasks[10].Answer4 = "Тихий Дон (Михаил Шолохов)";
            Tasks[10].RightAnswer = "1";

            Tasks[11].Question = "Главной героиней какого из нижеперечисленных произведений является Анна Раневская?";
            Tasks[11].Answer1 = "Старуха Изергиль (Максим Горький)";
            Tasks[11].Answer2 = "Вишневый сад (Антон Чехов)";
            Tasks[11].Answer3 = "Горе от ума (Александр Грибоедов)";
            Tasks[11].Answer4 = "Анна Каренина (Лев Толстой)";
            Tasks[11].RightAnswer = "2";

        }

        
    }
}
