﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFA_MushroomGame
{

    public partial class Form1 : Form
    {
        MainMushroom playerBack = new MainMushroom(); // инициализация игрока и ежиков
        Hedgehog Hedgehog1Back = new Hedgehog();
        Hedgehog Hedgehog2Back = new Hedgehog();
        public  PictureBox[] Floors = new PictureBox[24];
        public PictureBox[] Waters = new PictureBox[3];
        public Form1()
        {
            InitializeComponent();
            InitializeObjects();
            Timer GameTimer = new Timer();
            DoubleBuffered = true; // более ровная отрисовка при движении

            MainForm = this; // нужно для обращения к форме извне

            Point PlayerStartingLocation = new Point(60, 900); //стартовое положение игрока
            playerBack.Location = PlayerStartingLocation;
            player.Size = new Size (playerBack.SizeX, playerBack.SizeY) ;
            player.Location = playerBack.Location;
            player.BackgroundImage = playerBack.SpriteFile;

            Hedgehog1Back.StartingPoint = new Point(710, 600);
            Hedgehog2Back.StartingPoint = new Point(450, 265);
            Hedgehog1.Location = Hedgehog1Back.StartingPoint;
            Hedgehog2.Location = Hedgehog2Back.StartingPoint;

            GameTimer.Start(); // запуск игрового таймера
            GameTimer.Interval = 100; 
            GameTimer.Tick += GameTimer_Tick;

            KeyDown += Form1_KeyDown;

        }

        public static Form1 MainForm { get; set; } // реализация обращения к форме из других классов

        public void GameTimer_Tick(object sender , EventArgs e) //гравитация
        {
            playerBack.jumpspeed-=2;
            Point TempLocation = new Point(player.Location.X, (player.Location.Y - playerBack.jumpspeed));
            if (!playerBack.CheckCollisions(Floors, Waters, Hedgehog1, Hedgehog2, TempLocation, Final)) {
                playerBack.Location = TempLocation;
                player.Location = TempLocation;
                playerBack.InAir = true;
            }
            else {
                playerBack.InAir = false;
                playerBack.jumpspeed = 0;
            }
            HedgehogMovement(Hedgehog1, Hedgehog1Back, 980); //движение ёжика
            HedgehogMovement(Hedgehog2, Hedgehog2Back, 865);
            if (playerBack.FallingOut(player, player.Location)) {
                DialogResult dialog = MessageBox.Show("Вы выпали за пределы карты, попробуйте ещё раз!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                this.Close();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode) {
                case Keys.W:
                    if (!playerBack.InAir)
                        playerBack.jumpspeed = 26;
                    break;
                case Keys.A:
                    player.Location = playerBack.PlayerMovement("Left", Floors, Waters, Hedgehog1, Hedgehog2, Final);
                    break;
                case Keys.S:
                    player.Location = playerBack.PlayerMovement("Down", Floors, Waters, Hedgehog1, Hedgehog2, Final);
                    break;
                case Keys.D:
                    player.Location = playerBack.PlayerMovement("Right", Floors, Waters, Hedgehog1, Hedgehog2, Final);
                    break;
            }
        }


        private void HedgehogMovement(PictureBox hedgehog, Hedgehog hedgehogBack, int endpoint_location) //движение ежей
        {
            if (hedgehogBack.RightDirection) {
                if (hedgehog.Location.X < endpoint_location) // до конечной точки
                    hedgehog.Location = new Point(hedgehog.Location.X + hedgehogBack.Speed, hedgehog.Location.Y);
                else {
                    hedgehogBack.RightDirection = false;
                    hedgehog.BackgroundImage.RotateFlip(RotateFlipType.RotateNoneFlipX); // разворот спрайта в обратную сторону
                    hedgehog.Invalidate();
                    
                }
            }
            else {
                if (hedgehog.Location.X > hedgehogBack.StartingPoint.X) // до стартовой позиции
                    hedgehog.Location = new Point(hedgehog.Location.X - hedgehogBack.Speed, hedgehog.Location.Y);
                else {
                    hedgehogBack.RightDirection = true;
                    hedgehog.BackgroundImage.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    hedgehog.Invalidate();
                }
            }
        }

        private void InitializeObjects()
        {
            Floors[0] = Floor1;
            Floors[1] = Floor2;
            Floors[2] = floor3;
            Floors[3] = floor4;
            Floors[4] = floor5;
            Floors[5] = floor6;
            Floors[6] = floor7;
            Floors[7] = floor8;
            Floors[8] = floor9;
            Floors[9] = floor10;
            Floors[10] = floor11;
            Floors[11] = floor12;
            Floors[12] = floor13;
            Floors[13] = floor14;
            Floors[14] = wood1;
            Floors[15] = wood2;
            Floors[16] = wood3;
            Floors[17] = wood4;
            Floors[18] = wood5;
            Floors[19] = wood6;
            Floors[20] = wood7;
            Floors[21] = wood8;
            Floors[22] = wood9;
            Floors[23] = wood10;

            Waters[0] = water1;
            Waters[1] = water2;
            Waters[2] = water3;
        }



        private void BackgroundBox_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }


        
    }
}
