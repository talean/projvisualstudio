﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFA_MushroomGame
{
    public class GameObject
    {
        public GameObject()
        {

        }
        public GameObject(System.Drawing.Point Position, int sizeX, int sizeY, System.Drawing.Image FromResources) //не используется
        {
            this.Location = Position;
            this.SizeX = sizeX;
            this.SizeY = sizeY;
            SpriteFile = FromResources;
            Speed = 0;
        }

        public System.Drawing.Point Location { get; set; } // расположение объекта на экране
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public System.Drawing.Image SpriteFile { get; set; } // путь к расположению спрайта
        public int Speed { get; set; }
        public System.Drawing.Rectangle Hitbox { get; set; }


        
    }
}
