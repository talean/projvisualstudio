﻿namespace WFA_MushroomGame
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.player = new System.Windows.Forms.PictureBox();
            this.Floor1 = new System.Windows.Forms.PictureBox();
            this.Floor2 = new System.Windows.Forms.PictureBox();
            this.floor3 = new System.Windows.Forms.PictureBox();
            this.floor4 = new System.Windows.Forms.PictureBox();
            this.floor5 = new System.Windows.Forms.PictureBox();
            this.water1 = new System.Windows.Forms.PictureBox();
            this.wood1 = new System.Windows.Forms.PictureBox();
            this.floor6 = new System.Windows.Forms.PictureBox();
            this.floor7 = new System.Windows.Forms.PictureBox();
            this.floor8 = new System.Windows.Forms.PictureBox();
            this.Hedgehog1 = new System.Windows.Forms.PictureBox();
            this.floor9 = new System.Windows.Forms.PictureBox();
            this.floor10 = new System.Windows.Forms.PictureBox();
            this.wood2 = new System.Windows.Forms.PictureBox();
            this.wood3 = new System.Windows.Forms.PictureBox();
            this.floor11 = new System.Windows.Forms.PictureBox();
            this.floor12 = new System.Windows.Forms.PictureBox();
            this.water2 = new System.Windows.Forms.PictureBox();
            this.Hedgehog2 = new System.Windows.Forms.PictureBox();
            this.wood5 = new System.Windows.Forms.PictureBox();
            this.floor13 = new System.Windows.Forms.PictureBox();
            this.floor14 = new System.Windows.Forms.PictureBox();
            this.Final = new System.Windows.Forms.PictureBox();
            this.water3 = new System.Windows.Forms.PictureBox();
            this.wood10 = new System.Windows.Forms.PictureBox();
            this.wood9 = new System.Windows.Forms.PictureBox();
            this.wood8 = new System.Windows.Forms.PictureBox();
            this.wood7 = new System.Windows.Forms.PictureBox();
            this.wood6 = new System.Windows.Forms.PictureBox();
            this.wood4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Floor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Floor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.water1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hedgehog1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.water2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hedgehog2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Final)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.water3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood4)).BeginInit();
            this.SuspendLayout();
            // 
            // player
            // 
            this.player.BackColor = System.Drawing.Color.Transparent;
            this.player.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("player.BackgroundImage")));
            this.player.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.player.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.player.Location = new System.Drawing.Point(62, 858);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(65, 65);
            this.player.TabIndex = 1;
            this.player.TabStop = false;
            this.player.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Floor1
            // 
            this.Floor1.BackColor = System.Drawing.Color.Transparent;
            this.Floor1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Floor1.BackgroundImage")));
            this.Floor1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Floor1.Location = new System.Drawing.Point(-163, 970);
            this.Floor1.Name = "Floor1";
            this.Floor1.Size = new System.Drawing.Size(274, 64);
            this.Floor1.TabIndex = 2;
            this.Floor1.TabStop = false;
            // 
            // Floor2
            // 
            this.Floor2.BackColor = System.Drawing.Color.Transparent;
            this.Floor2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Floor2.BackgroundImage")));
            this.Floor2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Floor2.Location = new System.Drawing.Point(107, 970);
            this.Floor2.Name = "Floor2";
            this.Floor2.Size = new System.Drawing.Size(274, 64);
            this.Floor2.TabIndex = 3;
            this.Floor2.TabStop = false;
            // 
            // floor3
            // 
            this.floor3.BackColor = System.Drawing.Color.Transparent;
            this.floor3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor3.BackgroundImage")));
            this.floor3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor3.Location = new System.Drawing.Point(449, 970);
            this.floor3.Name = "floor3";
            this.floor3.Size = new System.Drawing.Size(274, 64);
            this.floor3.TabIndex = 4;
            this.floor3.TabStop = false;
            // 
            // floor4
            // 
            this.floor4.BackColor = System.Drawing.Color.Transparent;
            this.floor4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor4.BackgroundImage")));
            this.floor4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor4.Location = new System.Drawing.Point(808, 909);
            this.floor4.Name = "floor4";
            this.floor4.Size = new System.Drawing.Size(274, 64);
            this.floor4.TabIndex = 5;
            this.floor4.TabStop = false;
            // 
            // floor5
            // 
            this.floor5.BackColor = System.Drawing.Color.Transparent;
            this.floor5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor5.BackgroundImage")));
            this.floor5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor5.Location = new System.Drawing.Point(1158, 846);
            this.floor5.Name = "floor5";
            this.floor5.Size = new System.Drawing.Size(274, 64);
            this.floor5.TabIndex = 6;
            this.floor5.TabStop = false;
            // 
            // water1
            // 
            this.water1.BackColor = System.Drawing.Color.Transparent;
            this.water1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("water1.BackgroundImage")));
            this.water1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.water1.Location = new System.Drawing.Point(1429, 846);
            this.water1.Name = "water1";
            this.water1.Size = new System.Drawing.Size(274, 64);
            this.water1.TabIndex = 7;
            this.water1.TabStop = false;
            // 
            // wood1
            // 
            this.wood1.BackColor = System.Drawing.Color.Transparent;
            this.wood1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood1.BackgroundImage")));
            this.wood1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood1.Location = new System.Drawing.Point(1507, 714);
            this.wood1.Name = "wood1";
            this.wood1.Size = new System.Drawing.Size(163, 47);
            this.wood1.TabIndex = 9;
            this.wood1.TabStop = false;
            // 
            // floor6
            // 
            this.floor6.BackColor = System.Drawing.Color.Transparent;
            this.floor6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor6.BackgroundImage")));
            this.floor6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor6.Location = new System.Drawing.Point(1699, 846);
            this.floor6.Name = "floor6";
            this.floor6.Size = new System.Drawing.Size(274, 64);
            this.floor6.TabIndex = 10;
            this.floor6.TabStop = false;
            // 
            // floor7
            // 
            this.floor7.BackColor = System.Drawing.Color.Transparent;
            this.floor7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor7.BackgroundImage")));
            this.floor7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor7.Location = new System.Drawing.Point(1100, 607);
            this.floor7.Name = "floor7";
            this.floor7.Size = new System.Drawing.Size(274, 64);
            this.floor7.TabIndex = 11;
            this.floor7.TabStop = false;
            // 
            // floor8
            // 
            this.floor8.BackColor = System.Drawing.Color.Transparent;
            this.floor8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor8.BackgroundImage")));
            this.floor8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor8.Location = new System.Drawing.Point(884, 667);
            this.floor8.Name = "floor8";
            this.floor8.Size = new System.Drawing.Size(274, 64);
            this.floor8.TabIndex = 12;
            this.floor8.TabStop = false;
            // 
            // Hedgehog1
            // 
            this.Hedgehog1.BackColor = System.Drawing.Color.Transparent;
            this.Hedgehog1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Hedgehog1.BackgroundImage")));
            this.Hedgehog1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Hedgehog1.Location = new System.Drawing.Point(700, 600);
            this.Hedgehog1.Name = "Hedgehog1";
            this.Hedgehog1.Size = new System.Drawing.Size(100, 70);
            this.Hedgehog1.TabIndex = 13;
            this.Hedgehog1.TabStop = false;
            // 
            // floor9
            // 
            this.floor9.BackColor = System.Drawing.Color.Transparent;
            this.floor9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor9.BackgroundImage")));
            this.floor9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor9.Location = new System.Drawing.Point(669, 667);
            this.floor9.Name = "floor9";
            this.floor9.Size = new System.Drawing.Size(274, 64);
            this.floor9.TabIndex = 14;
            this.floor9.TabStop = false;
            // 
            // floor10
            // 
            this.floor10.BackColor = System.Drawing.Color.Transparent;
            this.floor10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor10.BackgroundImage")));
            this.floor10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor10.Location = new System.Drawing.Point(429, 607);
            this.floor10.Name = "floor10";
            this.floor10.Size = new System.Drawing.Size(274, 64);
            this.floor10.TabIndex = 15;
            this.floor10.TabStop = false;
            // 
            // wood2
            // 
            this.wood2.BackColor = System.Drawing.Color.Transparent;
            this.wood2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood2.BackgroundImage")));
            this.wood2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood2.Location = new System.Drawing.Point(260, 607);
            this.wood2.Name = "wood2";
            this.wood2.Size = new System.Drawing.Size(163, 47);
            this.wood2.TabIndex = 16;
            this.wood2.TabStop = false;
            // 
            // wood3
            // 
            this.wood3.BackColor = System.Drawing.Color.Transparent;
            this.wood3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood3.BackgroundImage")));
            this.wood3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood3.Location = new System.Drawing.Point(53, 473);
            this.wood3.Name = "wood3";
            this.wood3.Size = new System.Drawing.Size(163, 47);
            this.wood3.TabIndex = 17;
            this.wood3.TabStop = false;
            // 
            // floor11
            // 
            this.floor11.BackColor = System.Drawing.Color.Transparent;
            this.floor11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor11.BackgroundImage")));
            this.floor11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor11.Location = new System.Drawing.Point(429, 334);
            this.floor11.Name = "floor11";
            this.floor11.Size = new System.Drawing.Size(274, 64);
            this.floor11.TabIndex = 18;
            this.floor11.TabStop = false;
            // 
            // floor12
            // 
            this.floor12.BackColor = System.Drawing.Color.Transparent;
            this.floor12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor12.BackgroundImage")));
            this.floor12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor12.Location = new System.Drawing.Point(699, 334);
            this.floor12.Name = "floor12";
            this.floor12.Size = new System.Drawing.Size(274, 64);
            this.floor12.TabIndex = 19;
            this.floor12.TabStop = false;
            // 
            // water2
            // 
            this.water2.BackColor = System.Drawing.Color.Transparent;
            this.water2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("water2.BackgroundImage")));
            this.water2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.water2.Location = new System.Drawing.Point(970, 334);
            this.water2.Name = "water2";
            this.water2.Size = new System.Drawing.Size(274, 64);
            this.water2.TabIndex = 20;
            this.water2.TabStop = false;
            // 
            // Hedgehog2
            // 
            this.Hedgehog2.BackColor = System.Drawing.Color.Transparent;
            this.Hedgehog2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Hedgehog2.BackgroundImage")));
            this.Hedgehog2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Hedgehog2.Location = new System.Drawing.Point(864, 258);
            this.Hedgehog2.Name = "Hedgehog2";
            this.Hedgehog2.Size = new System.Drawing.Size(100, 70);
            this.Hedgehog2.TabIndex = 21;
            this.Hedgehog2.TabStop = false;
            // 
            // wood5
            // 
            this.wood5.BackColor = System.Drawing.Color.Transparent;
            this.wood5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood5.BackgroundImage")));
            this.wood5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood5.Location = new System.Drawing.Point(1036, 214);
            this.wood5.Name = "wood5";
            this.wood5.Size = new System.Drawing.Size(163, 47);
            this.wood5.TabIndex = 22;
            this.wood5.TabStop = false;
            // 
            // floor13
            // 
            this.floor13.BackColor = System.Drawing.Color.Transparent;
            this.floor13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor13.BackgroundImage")));
            this.floor13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor13.Location = new System.Drawing.Point(1507, 334);
            this.floor13.Name = "floor13";
            this.floor13.Size = new System.Drawing.Size(274, 64);
            this.floor13.TabIndex = 24;
            this.floor13.TabStop = false;
            // 
            // floor14
            // 
            this.floor14.BackColor = System.Drawing.Color.Transparent;
            this.floor14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("floor14.BackgroundImage")));
            this.floor14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.floor14.Location = new System.Drawing.Point(1773, 334);
            this.floor14.Name = "floor14";
            this.floor14.Size = new System.Drawing.Size(274, 64);
            this.floor14.TabIndex = 25;
            this.floor14.TabStop = false;
            // 
            // Final
            // 
            this.Final.BackColor = System.Drawing.Color.Transparent;
            this.Final.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Final.BackgroundImage")));
            this.Final.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Final.Location = new System.Drawing.Point(1670, 88);
            this.Final.Name = "Final";
            this.Final.Size = new System.Drawing.Size(139, 310);
            this.Final.TabIndex = 26;
            this.Final.TabStop = false;
            // 
            // water3
            // 
            this.water3.BackColor = System.Drawing.Color.Transparent;
            this.water3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("water3.BackgroundImage")));
            this.water3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.water3.Location = new System.Drawing.Point(1241, 334);
            this.water3.Name = "water3";
            this.water3.Size = new System.Drawing.Size(274, 64);
            this.water3.TabIndex = 27;
            this.water3.TabStop = false;
            // 
            // wood10
            // 
            this.wood10.BackColor = System.Drawing.Color.Transparent;
            this.wood10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood10.BackgroundImage")));
            this.wood10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood10.Location = new System.Drawing.Point(1419, 394);
            this.wood10.Name = "wood10";
            this.wood10.Size = new System.Drawing.Size(163, 47);
            this.wood10.TabIndex = 28;
            this.wood10.TabStop = false;
            // 
            // wood9
            // 
            this.wood9.BackColor = System.Drawing.Color.Transparent;
            this.wood9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood9.BackgroundImage")));
            this.wood9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood9.Location = new System.Drawing.Point(1260, 394);
            this.wood9.Name = "wood9";
            this.wood9.Size = new System.Drawing.Size(163, 47);
            this.wood9.TabIndex = 29;
            this.wood9.TabStop = false;
            // 
            // wood8
            // 
            this.wood8.BackColor = System.Drawing.Color.Transparent;
            this.wood8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood8.BackgroundImage")));
            this.wood8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood8.Location = new System.Drawing.Point(1100, 394);
            this.wood8.Name = "wood8";
            this.wood8.Size = new System.Drawing.Size(163, 47);
            this.wood8.TabIndex = 30;
            this.wood8.TabStop = false;
            // 
            // wood7
            // 
            this.wood7.BackColor = System.Drawing.Color.Transparent;
            this.wood7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood7.BackgroundImage")));
            this.wood7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood7.Location = new System.Drawing.Point(941, 394);
            this.wood7.Name = "wood7";
            this.wood7.Size = new System.Drawing.Size(163, 47);
            this.wood7.TabIndex = 31;
            this.wood7.TabStop = false;
            // 
            // wood6
            // 
            this.wood6.BackColor = System.Drawing.Color.Transparent;
            this.wood6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood6.BackgroundImage")));
            this.wood6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood6.Location = new System.Drawing.Point(1305, 156);
            this.wood6.Name = "wood6";
            this.wood6.Size = new System.Drawing.Size(163, 47);
            this.wood6.TabIndex = 32;
            this.wood6.TabStop = false;
            // 
            // wood4
            // 
            this.wood4.BackColor = System.Drawing.Color.Transparent;
            this.wood4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wood4.BackgroundImage")));
            this.wood4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wood4.Location = new System.Drawing.Point(260, 334);
            this.wood4.Name = "wood4";
            this.wood4.Size = new System.Drawing.Size(163, 47);
            this.wood4.TabIndex = 33;
            this.wood4.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1878, 1032);
            this.Controls.Add(this.wood4);
            this.Controls.Add(this.wood6);
            this.Controls.Add(this.wood7);
            this.Controls.Add(this.wood8);
            this.Controls.Add(this.wood9);
            this.Controls.Add(this.wood10);
            this.Controls.Add(this.water3);
            this.Controls.Add(this.floor14);
            this.Controls.Add(this.floor13);
            this.Controls.Add(this.wood5);
            this.Controls.Add(this.Hedgehog2);
            this.Controls.Add(this.water2);
            this.Controls.Add(this.floor12);
            this.Controls.Add(this.floor11);
            this.Controls.Add(this.wood3);
            this.Controls.Add(this.wood2);
            this.Controls.Add(this.floor10);
            this.Controls.Add(this.floor9);
            this.Controls.Add(this.Hedgehog1);
            this.Controls.Add(this.floor8);
            this.Controls.Add(this.floor7);
            this.Controls.Add(this.floor6);
            this.Controls.Add(this.wood1);
            this.Controls.Add(this.water1);
            this.Controls.Add(this.floor5);
            this.Controls.Add(this.floor4);
            this.Controls.Add(this.floor3);
            this.Controls.Add(this.Floor2);
            this.Controls.Add(this.Floor1);
            this.Controls.Add(this.player);
            this.Controls.Add(this.Final);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Floor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Floor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.water1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hedgehog1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.water2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hedgehog2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Final)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.water3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wood4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.PictureBox player;
        public System.Windows.Forms.PictureBox Floor1;
        public System.Windows.Forms.PictureBox Floor2;
        public System.Windows.Forms.PictureBox floor3;
        public System.Windows.Forms.PictureBox wood9;
        public System.Windows.Forms.PictureBox pics3;
        public System.Windows.Forms.PictureBox water1;
        public System.Windows.Forms.PictureBox wood1;
        public System.Windows.Forms.PictureBox floor6;
        public System.Windows.Forms.PictureBox floor7;
        public System.Windows.Forms.PictureBox floor8;
        public System.Windows.Forms.PictureBox Hedgehog1;
        public System.Windows.Forms.PictureBox floor9;
        public System.Windows.Forms.PictureBox floor10;
        public System.Windows.Forms.PictureBox wood2;
        public System.Windows.Forms.PictureBox wood3;
        public System.Windows.Forms.PictureBox floor11;
        public System.Windows.Forms.PictureBox floor12;
        public System.Windows.Forms.PictureBox water2;
        public System.Windows.Forms.PictureBox Hedgehog2;
        public System.Windows.Forms.PictureBox wood5;
        public System.Windows.Forms.PictureBox floor13;
        public System.Windows.Forms.PictureBox floor14;
        public System.Windows.Forms.PictureBox Final;
        public System.Windows.Forms.PictureBox water3;
        public System.Windows.Forms.PictureBox floor4;
        public System.Windows.Forms.PictureBox floor5;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.PictureBox wood8;
        public System.Windows.Forms.PictureBox wood7;
        public System.Windows.Forms.PictureBox wood6;
        public System.Windows.Forms.PictureBox wood4;
        public System.Windows.Forms.PictureBox wood10;
    }
}

