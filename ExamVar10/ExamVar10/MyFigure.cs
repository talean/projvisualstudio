﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
namespace ExamVar10
{
    class MyFigure
    {
        public MyFigure()
        {
            
        }
        public string FigureType { get; set; }
        public Color FigureColor { get; set; }
        public Point StartingPoint { get; set; }
        public Size FigureSize { get; set; }
        public Rectangle Body { get; set; }

        public void updateBody()
        {
            Body = new Rectangle(StartingPoint, FigureSize);
        }

       
    }
}
