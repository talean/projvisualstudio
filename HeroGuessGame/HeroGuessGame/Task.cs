﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeroGuessGame
{
    class Task
    {
        public Task()
        {
        
        }
        public string Question { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string RightAnswer { get; set; }

    }
}
