﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WFA_MushroomGame
{
    class MainMushroom : GameObject
    {
        public MainMushroom()
        {
            this.Location = new System.Drawing.Point(60, 900); // начальное положение игрока
            this.SizeX = 60;
            this.SizeY = 60;
            this.Speed = 12;
            this.SpriteFile = Properties.Resources.Mushroom1;
            this.jumpspeed = 0;

        }

        public int jumpspeed { get; set; }
        public bool InAir { get; set; }
        public bool CheckCollisions(PictureBox[] Floors, PictureBox[] Waters, PictureBox H1, PictureBox H2, System.Drawing.Point TempLocation, PictureBox Final)
        {
            
            foreach (PictureBox floor in Floors)  // collisions
            {
                Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
                System.Drawing.Rectangle wallRect = new System.Drawing.Rectangle(floor.Location.X, floor.Location.Y, floor.Width, floor.Height);
                if (wallRect.IntersectsWith(Hitbox))
                    return true;
            }

            foreach (PictureBox water in Waters) 
            {
                Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
                System.Drawing.Rectangle waterRect = new System.Drawing.Rectangle(water.Location.X, water.Location.Y, water.Width, water.Height);
                if (waterRect.IntersectsWith(Hitbox)) {

                    DialogResult dialog = MessageBox.Show("Вы попали в воду! Придется начать сначала!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                    Form1.MainForm.Close();
                }
            }

            Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
            System.Drawing.Rectangle H1Rect = new System.Drawing.Rectangle(H1.Location.X, H1.Location.Y, H1.Width, H1.Height);
            if (H1Rect.IntersectsWith(Hitbox)) {
                DialogResult dialog = MessageBox.Show("Вы попались ежику! Придется начать сначала!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                Form1.MainForm.Close();
            }
                

            Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
            System.Drawing.Rectangle H2Rect = new System.Drawing.Rectangle(H2.Location.X, H2.Location.Y, H2.Width, H2.Height);
            if (H2Rect.IntersectsWith(Hitbox)) {
               DialogResult dialog = MessageBox.Show("Вы попались ежику! Придется начать сначала!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
               Form1.MainForm.Close();
            }

            Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
            System.Drawing.Rectangle FinalRect = new System.Drawing.Rectangle(Final.Location.X, Final.Location.Y, Final.Width, Final.Height);
            if (FinalRect.IntersectsWith(Hitbox)) {
                DialogResult dialog = MessageBox.Show("Вы успешно добрались! Победа!", "Вы выиграли!", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                Form1.MainForm.Close();
            }

            return false;
        }

        public System.Drawing.Point PlayerMovement(string Direction, PictureBox[] Floors, PictureBox[] Waters, PictureBox H1, PictureBox H2, PictureBox Final)
        {
            System.Drawing.Point TempLocation=Location;

            switch (Direction) {
                default:
                    break;

                case "Down":
                    TempLocation = new System.Drawing.Point(this.Location.X, this.Location.Y + this.Speed);
                    Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
                    if (CheckCollisions(Floors, Waters, H1, H2, TempLocation, Final)) {
                        break;
                    }
                    else {
                        Location = TempLocation;
                        break;
                    }

                case "Left":
                    TempLocation = new System.Drawing.Point(this.Location.X - this.Speed, this.Location.Y);
                    Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
                    if (CheckCollisions(Floors, Waters, H1, H2, TempLocation, Final)) {
                        break;
                    }
                    else {
                        Location = TempLocation;
                        break;
                    }

                case "Right":
                    TempLocation = new System.Drawing.Point(this.Location.X + this.Speed, this.Location.Y);
                    Hitbox = new System.Drawing.Rectangle(TempLocation, new System.Drawing.Size(SizeX, SizeY));
                    if (CheckCollisions(Floors, Waters, H1, H2, TempLocation, Final)) {
                        break;
                    }
                    else {
                        Location = TempLocation;
                        break;
                    }
            }
            return Location;
        }

        public bool FallingOut(PictureBox player, System.Drawing.Point TempLocation) //проверка предела экрана
        {
            System.Drawing.Rectangle playerRect = new System.Drawing.Rectangle(TempLocation, player.Size);
            System.Drawing.Rectangle formRec = new System.Drawing.Rectangle(-player.Width, -player.Height, Form1.MainForm.ClientSize.Width , Form1.MainForm.ClientSize.Height + 2*player.Height);
            return !playerRect.IntersectsWith(formRec);
        }
    }
}
