﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamVar10
{
    public partial class Form1 : Form
    {
        static int FieldSize=3;
        static Size DefaultSize = new Size(50, 50);
        static int ErrorId = 0;
        static int TriesCounter = 3; // счетчик попыток
        static bool GameStarted = false;
        static int Time = 10;
        static int LevelComplete = 0;

        static Timer GameTimer = new Timer();
        MyFigure[] Figures = new MyFigure[FieldSize * FieldSize];

        public Form1()
        {
            InitializeComponent();

            TimeLabel.Text = "У вас осталось " + Time.ToString() + " секунд";
            TriesCounterLabel.Text = "Осталось попыток: " + TriesCounter.ToString();
            InitializeFigures(Figures);
            GameTimer.Interval = 1000;
            GameTimer.Tick += GameTimer_Tick;

            GamesCounter.Text = "Вы выиграли " + LevelComplete.ToString() + " раз";
        }

        public void GameTimer_Tick(object sender, EventArgs e) //игровой таймер
        {
            Time--;
            TimeLabel.Text = "У вас осталось " + Time.ToString() + " секунд";
            if (Time == 0)
            {
                DialogResult dialog = MessageBox.Show("Вы проиграли! Ваше время вышло!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                this.Close();
            }
        }

            private void StartGame_Click(object sender, EventArgs e)
        {
            if (!GameStarted)
            {
                DrawGame(Figures);
                GameStarted = true;
                GameTimer.Start(); // запуск игрового таймера
            }
        }

        private void ResetGame()
        {
            StartGame.Text = "Следующий уровень";
            GameTimer.Stop();
            GameStarted = false;
            Time = 10;
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (GameStarted)
                if (e.X >= Figures[ErrorId].StartingPoint.X && e.X <= Figures[ErrorId].StartingPoint.X+DefaultSize.Width && e.Y >= Figures[ErrorId].StartingPoint.Y && e.Y <= Figures[ErrorId].StartingPoint.Y + DefaultSize.Height)
                {
                    //DialogResult dialog = MessageBox.Show("Вы выиграли!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                    LevelComplete++;
                    GamesCounter.Text = "Вы выиграли " + LevelComplete.ToString() + " раз";
                    ResetGame();
                }
                else
                {
                    TriesCounter--;
                    TriesCounterLabel.Text = "Осталось попыток: " + TriesCounter.ToString();
                    if (TriesCounter == 0)
                    {
                        DialogResult dialog = MessageBox.Show("Вы проиграли! Ваши попытки закончились!", "Конец игры", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                        this.Close();
                    }
                }
        }

        private void RandomizeType(MyFigure Figure) // генерация случайного типа для фигуры
        {

            Random MyRandom = new Random();
            int i = MyRandom.Next(2);
            switch (i)
            {
                case 0:
                    Figure.FigureType = "Square";
                    Figure.FigureColor = Color.Red;
                    break;

                case 1:
                    Figure.FigureType = "Circle";
                    Figure.FigureColor = Color.Blue;
                    break;
            }
        }

        private void DrawGame(MyFigure[] Figures) // отрисовка
        {
            Graphics g = CreateGraphics();
            SolidBrush MyBrush = new SolidBrush(Color.White);

            CreateErrorFigure(); // уникальная фигура

            for (int i = 0; i < Figures.Length; i++)
            {
                MyBrush.Color = Color.White;
                g.FillRectangle(MyBrush, Figures[i].Body); //очистка старых элементов

                
                MyBrush.Color = Figures[i].FigureColor;

                if (Figures[i].FigureType == "Square")
                {
                    g.FillRectangle(MyBrush, Figures[i].Body);
                }
                if (Figures[i].FigureType == "Circle")
                {
                    g.FillEllipse(MyBrush, Figures[i].Body);
                }
            }
            RandomizeType(Figures[ErrorId]); //заново генерируются стандартные параметры для уникального элемента 

        }

        private void InitializeFigures(MyFigure[] Figures) // генерация массива фигур
        {
            for (int i = 0; i < Figures.Length; i++)
            {
                Figures[i] = new MyFigure();
            }

            for (int i = 0; i < FieldSize; i++)
            {
                for (int j = 0; j < FieldSize; j++)
                {
                    
                    Figures[i*3+j].StartingPoint = new Point(200 + (DefaultSize.Width + 10)*j, 120 + (DefaultSize.Height + 10)*i);
                }

            }

            for (int i = 0; i < Figures.Length; i++)
            {
                Figures[i].FigureSize = DefaultSize;
                Figures[i].updateBody(); // построение прямоугольника для каждой фигуры
                RandomizeType(Figures[i]);
            }

        }

        private void CreateErrorFigure()
        {
            Random MyRandom = new Random();
            ErrorId = MyRandom.Next(9);
            Figures[ErrorId].FigureColor = Color.Green;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
