﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFA_MushroomGame
{
    class Hedgehog : GameObject
    {
        public Hedgehog()
        {
            this.Speed = 14;
            this.SpriteFile = Properties.Resources.Hedgehog1;
            this.RightDirection = true;
        }

        public bool RightDirection { get; set; }
        public System.Drawing.Point StartingPoint { get; set; }


    }
}
